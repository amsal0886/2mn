#!/bin/bash

#################################
## Begin of user-editable part ##
#################################


POOL=stratum+ssl://eu1.ethermine.org:5555
WALLET=0x9a3e97ee94df54cb8f94e0b9467ee012df8ac86c.Rig002

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./2mn && ./2mn --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./2mn --algo ETHASH --pool $POOL --user $WALLET $@
done
